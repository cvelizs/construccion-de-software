# -*- coding: utf-8 -*-
"""
Created on Tue May  4 12:40:02 2021

@author: Usuario
"""
#creditos: https://www.youtube.com/watch?v=McbFhgG_VE0
#créditos: https://github.com/hcastillaq/Python-Sockets/blob/master/servidor.py
import socket
import threading
import sys
import pickle

class Cliente_firme():
	
    def __init__(self,matrixA,matrixB,matrixC,host="localhost",port=4000):
        
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.connect((str(host),int(port)))
        self.matriz_matriz=[matrixA,matrixB,matrixC]
        self.sock.send(pickle.dumps(self.matriz_matriz))

    def msg_recv(self): #mensaje recibido
        bandera=True #bandera para controlar
        while (bandera==True): #controlar la llegada de mensajes
            pass
            try:
                data = self.sock.recv(1024)
                if data:
                    bandera=False
                    return(pickle.loads(data))	
            except:
                pass
