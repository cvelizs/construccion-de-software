# -*- coding: utf-8 -*-
"""
Created on Tue May  4 22:25:36 2021

@author: Usuario
"""

from tkinter import *
from Cliente_firme import *
import threading
from time import time
from threading import Thread
import numpy as np
import random
from matplotlib import pyplot as plt
from concurrent.futures import ThreadPoolExecutor

raiz=Tk() #mi interfaz se llama raiz
raiz.title("Cliente")
raiz.resizable(0,0)#para modificar tamaño de ventana
#raiz.iconbitmap("l1.ico")#poner imagen, con .ico
raiz.geometry("400x220")
raiz.config(bg="pink")

matrizA_n=IntVar() #depende del tipo de dato
matrizA_m=IntVar()
matrizB_n=IntVar()
matrizB_m=IntVar()

def Llenar_matriz(matrizA_n,matrizA_m,matrizB_n,matrizB_m):
    global Matrix_A
    global Matrix_B
    global Matrix_C

    #genero matrices A y B con valores aleatorios entre 1 y 100 y la lamtriz C con ceros
    Matrix_A = np.random.randint(1, 100,(matrizA_n,matrizA_m) )
    Matrix_B = np.random.randint(1, 100, (matrizB_n,matrizB_m))
    Matrix_C = np.zeros((matrizA_n,matrizB_m))

def obtencion_data():
    #matrizA_n.set(8) para mandar valores desde acá
    filasA=matrizA_n.get() #para obtener un dato
    columnasA=matrizA_m.get()
    filasB=matrizB_n.get()
    columnasB=matrizB_m.get()
    
    if filasA==0 or columnasA==0 or filasB==0 or columnasB==0:
        ceros=Tk()
        ceros.title("Error")
        ceros.resizable(0,0)
        ceros.geometry("380x70")
        ceros.config(bg="black")
        jumperror=Label(ceros,font=("Ink Free", 12), text="No se acepta dimensión 0")
        jumperror.place(x=80,y=15)
        exit() #sale de la funcion
        ceros.mainloop()
    
    if columnasA!=filasB:
        desigualdad=Tk()
        desigualdad.title("Error")
        desigualdad.resizable(0,0)
        desigualdad.geometry("400x70")
        desigualdad.config(bg="black")
        jumperror2=Label(desigualdad,font=("Ink Free", 12), text="Es imposible realizar la multiplicación")
        jumperror2.place(x=80,y=15)
        exit() #sale de la funcion
        ceros.mainloop()
    global cliente
    Llenar_matriz(filasA,columnasA,filasB,columnasB)
    cliente=Cliente_firme(Matrix_A,Matrix_B,Matrix_C)
    
def recibe_matriz(): #recibe la matriz resultante de la multiplicación
    recibe=cliente.msg_recv()
    print(recibe)
    recibir = Tk()
    recibir.title("Matriz C")
    recibir.config(bg="yellow")
    for filas in range(len(recibe)):
        for columnas in range(len(recibe[filas])):
            cell = Entry(recibir, width=10)
            cell.grid(padx=5, pady=5,row=filas, column=columnas)
            cell.insert(0, '{}'.format(recibe[filas][columnas]))
    recibir.mainloop()
    
#todo se desarrolla aquí
label3=Label(raiz,font=("Ink Free", 12), text="Matriz A")
label3.place(x=80,y=15)

label3=Label(raiz,font=("Ink Free", 12), text="Filas")
label3.place(x=50,y=60)

label3=Label(raiz,font=("Ink Free", 12), text="Columnas")
label3.place(x=50,y=90)

cuadro1=Entry(raiz,font=("Ink Free", 12),textvariable=matrizA_n) #hacer cuadros que reciben texto
cuadro1.place(x=150,y=60,width=20,height=20)

cuadro1=Entry(raiz,font=("Ink Free", 12),textvariable=matrizA_m) #hacer cuadros que reciben texto
cuadro1.place(x=150,y=90,width=20,height=20)

#-----------------------------------------------------------------

label3=Label(raiz,font=("Ink Free", 12), text="Matriz B")
label3.place(x=250,y=15)

label3=Label(raiz,font=("Ink Free", 12), text="Filas")
label3.place(x=220,y=60)

label3=Label(raiz,font=("Ink Free", 12), text="Columnas")
label3.place(x=220,y=90)

cuadro1=Entry(raiz,font=("Ink Free", 12),textvariable=matrizB_n) #hacer cuadros que reciben texto
cuadro1.place(x=320,y=60,width=20,height=20)

cuadro1=Entry(raiz,font=("Ink Free", 12),textvariable=matrizB_m) #hacer cuadros que reciben texto
cuadro1.place(x=320,y=90,width=20,height=20)

#------------------------------------------------------------------
envio=Button(raiz,font=("Ink Free", 12),text="Enviar",command=obtencion_data) #hacer botones
envio.place(x=110,y=150)

envio=Button(raiz,font=("Ink Free", 12),text="Recibir resultado",command=recibe_matriz) #hacer botones
envio.place(x=190,y=150)

raiz.mainloop()