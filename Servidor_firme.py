#creditos: https://www.youtube.com/watch?v=McbFhgG_VE0
#creditos: https://github.com/hcastillaq/Python-Sockets/blob/master/servidor.py
#creditos: https://colab.research.google.com/drive/1cs594mDTB2p21_tvz9PnH9UEd7xl1DO7?usp=sharing#scrollTo=WSn32QoWAMUN
import socket
import threading
import sys
import pickle

class Servidor():
	def __init__(self, host="localhost", port=4000):

		self.clientes = []
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.bind((str(host), int(port)))
		self.sock.listen(10)
		self.sock.setblocking(False)
		
	def aceptarCon(self): #funcion para aceptar la conexion
		print("aceptarCon iniciado")
		bandera=True #bandera para que el while no sea infinito
		while bandera==True: #se pregunta cada vez que el socket se conecta
			try:
				conn, addr = self.sock.accept() #acepta conexion
				conn.setblocking(False) #evita que la conexion se bloquee
				self.clientes.append(conn) #se agrega a la lista de clientes
				bandera=False #la bandera se cambia a false
			except:
				pass

	def procesarCon(self): #funcion para procesar la conexion
		print("ProcesarCon iniciado")
		if len(self.clientes) > 0: #si la cantidad de clientes es mayor a 0
			for c in self.clientes: #para cada cliente
				try:
					data = c.recv(1024) #verifica que el mensaje se haya recibido
					if data: #si el mensaje es recibido
						return(pickle.loads(data)) #retorna el mensaje al servidor
				except:
					pass

	def msg_to_all(self, msg): #funcion para enviar el mensaje
		for c in self.clientes: #para cada cliente
			try:
				c.send(pickle.dumps(msg)) #retorna el mensaje a los clientes
			except:
				self.clientes.remove(c) #remover cada dirección para remover el mensaje
