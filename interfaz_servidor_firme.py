# -*- coding: utf-8 -*-
"""
Created on Tue May  4 22:25:36 2021

@author: Usuario
"""
#creditos: https://www.youtube.com/watch?v=McbFhgG_VE0
#créditos: https://github.com/hcastillaq/Python-Sockets/blob/master/servidor.py
#creditos: https://colab.research.google.com/drive/1cs594mDTB2p21_tvz9PnH9UEd7xl1DO7?usp=sharing#scrollTo=WSn32QoWAMUN

from tkinter import *
from Servidor_firme import *
#from tkinter import PhotoImage
#from PIL import ImageTk,Image
import threading
from time import time
from threading import Thread
import numpy as np
import random
from matplotlib import pyplot as plt
from concurrent.futures import ThreadPoolExecutor

server=Tk() #mi interfaz se llama raiz
server.title("Servidor")
server.resizable(0,0)#para modificar tamaño de ventana
#raiz.iconbitmap("l1.ico")#poner imagen, con .ico
server.geometry("400x250")
server.config(bg="pink")

    
dimensionesA=StringVar()
dimensionesB=StringVar()
numero_hilos=IntVar()


def Matrix_multiply_parallel(start,end,matrixa,matrixb,matrixc):
    dimension_N=len(matrixa)
    dimension_M=len(matrixa[0])
    dimension_F=len(matrixb[0])
    for i in range(start,end): #para que no choquen los threads
        for j in range(dimension_N):
            for k in range(dimension_N):
                matrixc[i][j] += int(matrixa[i][k] * matrixb[k][j])
    return(matrixc)

def Recibir_dimensiones_cliente():
    global procesa
    global servidor1
    servidor1=Servidor()
    servidor1.aceptarCon() #permite aceptar la conexion
    procesa=servidor1.procesarCon()
    dimensionesA.set(str(len(procesa[0]))+"x"+ str(len(procesa[0][0])))#(filasxcolumnas)
    dimensionesB.set(str(len(procesa[1]))+"x"+ str(len(procesa[1][0])))
    
def mostrar_matrizA():
    print(procesa[0])
    position0=procesa[0]
    primera_matriz = Tk() #crea nueva ventana
    primera_matriz.title("Matriz A")
    primera_matriz.config(bg="yellow")
    for fil in range(len(position0)):
        for col in range(len(position0[fil])): #por cada fila y columna, muestra los elementos
            cell = Entry(primera_matriz, width=10)
            cell.grid(padx=5, pady=5,row=fil, column=col)
            cell.insert(0, '{}'.format(position0[fil][col]))
    primera_matriz.mainloop()
    
def mostrar_matrizB():
    print(procesa[1])
    position1=procesa[1]
    segunda_matriz = Tk()
    segunda_matriz.title("Matriz B")
    segunda_matriz.config(bg="yellow")
    for fil in range(len(position1)):
        for col in range(len(position1[fil])):
            cell = Entry(segunda_matriz, width=10)
            cell.grid(padx=5, pady=5,row=fil, column=col)
            cell.insert(0, '{}'.format(position1[fil][col]))
    segunda_matriz.mainloop()
    
def threads():
    matrixa=procesa[0]
    matrixb=procesa[1]
    matrixc=procesa[2]
    dimension_N=len(matrixa)
    dimension_M=len(matrixa[0])
    num=numero_hilos.get()
    executor = ThreadPoolExecutor(max_workers=num)
    for j in range(num):
      task1 = executor.submit(Matrix_multiply_parallel, int((dimension_N/num) * j), int((dimension_M/num) * (j+1)),matrixa,matrixb,matrixc)
    matrixc=task1.result()
    tercera_matriz=matrixc
    servidor1.msg_to_all(tercera_matriz)
    
#todo se desarrolla aquí
label1=Label(server,font=("Ink Free", 12), text="Dimensiones de A")
label1.place(x=55,y=60)

cuadro1=Entry(server,font=("Ink Free", 11),textvariable=dimensionesA) #hacer cuadros que reciben texto
cuadro1.place(x=95,y=90,width=40,height=20)
#-----------------------------------------------------------------
label2=Label(server,font=("Ink Free", 12), text="Dimensiones de B")
label2.place(x=220,y=60)

cuadro2=Entry(server,font=("Ink Free", 11),textvariable=dimensionesB) #hacer cuadros que reciben texto
cuadro2.place(x=260,y=90,width=40,height=20)
#------------------------------------------------------------------
label3=Label(server,font=("Ink Free", 12), text="Número de hilos")
label3.place(x=110,y=125)

cuadro3=Entry(server,font=("Ink Free", 11),textvariable=numero_hilos) #hacer cuadros que reciben texto
cuadro3.place(x=240,y=128,width=40,height=20)
#------------------------------------------------------------------
recibirlas=Button(server,font=("Ink Free", 12),text="Establecer conexión con cliente",command=Recibir_dimensiones_cliente) #hacer botones
recibirlas.place(x=100,y=15)

primatriz=Button(server,font=("Ink Free", 12),text="Matriz A",command=mostrar_matrizA) #hacer botones
primatriz.place(x=77,y=165)

segmatriz=Button(server,font=("Ink Free", 12),text="Matriz B",command=mostrar_matrizB) #hacer botones
segmatriz.place(x=239,y=165)
#------------------------------------------------------------------
multiplicar=Button(server,font=("Ink Free", 12),text="Multiplicar",command=threads) #hacer botones
multiplicar.place(x=153,y=210)



server.mainloop()